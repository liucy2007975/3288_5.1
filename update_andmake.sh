#!/bin/bash

if [ $1 == "mipi" ]
	then
	{
		if [ $2 == "1920_1200" ]
			then
			{
				cp kernel/arch/arm/boot/dts/lcd-rpdzkj_mipi_10_1.dtsi kernel/arch/arm/boot/dts/lcd-rpdzkj.dtsi
			}
		elif [ $2 == "800_1280" ]
			then
			{
				cp kernel/arch/arm/boot/dts/lcd-rpdzkj_mipi_7.dtsi kernel/arch/arm/boot/dts/lcd-rpdzkj.dtsi
			}
		elif [ $2 == "720_1280" ]
			then
			{
				cp kernel/arch/arm/boot/dts/lcd-rpdzkj_mipi_5.dtsi kernel/arch/arm/boot/dts/lcd-rpdzkj.dtsi
			}
		else
			{
				cp kernel/arch/arm/boot/dts/lcd-rpdzkj_mipi_10_1.dtsi kernel/arch/arm/boot/dts/lcd-rpdzkj.dtsi
			}
		fi

	}
elif [ $1 == "lvds" ]
	then
	{
		cp kernel/arch/arm/boot/dts/lcd-rpdzkj_lvds.dtsi kernel/arch/arm/boot/dts/lcd-rpdzkj.dtsi
	}
elif [ $1 == "twolvds" ]
	then
	{
		cp kernel/arch/arm/boot/dts/lcd-rpdzkj_dual_lvds.dtsi kernel/arch/arm/boot/dts/lcd-rpdzkj.dtsi
	}
elif [ $1 == "edp" ]
	then
	{
		cp kernel/arch/arm/boot/dts/lcd-rpdzkj_edp.dtsi kernel/arch/arm/boot/dts/lcd-rpdzkj.dtsi
	}
else
	{
		cp kernel/arch/arm/boot/dts/lcd-rpdzkj_lvds.dtsi kernel/arch/arm/boot/dts/lcd-rpdzkj.dtsi
	}
fi
echo "cp  dtsi  OK!"
. bill_make.sh
. rpupdate.sh
