
#include <linux/i2c.h>
#include <linux/bcd.h>
#include <linux/rtc.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include <linux/delay.h>

unsigned char reg_val[][7]={	
	{0xFC,0xF3, 0x3B, 0x22,0xC8, 0x00,0x29},
	{0xFC,0xF3, 0x3B, 0x23,0x0D, 0x02,0x00},
	{0xFC,0xF3, 0x3B, 0x23,0x0C, 0x00,0x80},
	{0xFC,0xF3, 0x3B, 0x23,0x2F, 0xD7,0x00},
	{0xFC,0xF3, 0x3B, 0x23,0x32, 0x00,0x60},
	{0xFC,0xF3, 0x3B, 0x23,0x33, 0x00,0x10},
	{0xFC,0xF3, 0x3B, 0x23,0x37, 0xFF,0xF8},
	{0xFC,0xF3, 0x3B, 0x23,0x39, 0x00,0x10},
	{0xFC,0xF3, 0x3B, 0x23,0x48, 0x20,0x00},
	{0xFC,0xF3, 0x3B, 0x23,0xB3, 0x00,0x0C},
	{0xFC,0xF3, 0x3B, 0x23,0xB4, 0x00,0x04},
	{0xFC,0xF3, 0x3B, 0x23,0x05, 0x00,0x21},
	{0xFC,0xF3, 0x3B, 0x23,0x6E, 0x12,0x00},
	{0xFC,0xF3, 0x3B, 0x23,0x98, 0x00,0x80},
	{0xFC,0xF3, 0x3B, 0x23,0x04, 0x03,0xDE},
	{0xFC,0xF3, 0x3B, 0x23,0x72, 0x48,0x00},
	{0xFC,0xF3, 0x3B, 0x22,0xE5, 0x02,0x23},
	{0xFC,0xF3, 0x3B, 0x23,0x07, 0xF4,0xF4},
	{0xFC,0xF3, 0x3B, 0x23,0x08, 0x0A,0x13},
	{0xFC,0xF3, 0x3B, 0x23,0xB5, 0x60,0x00},
	{0xFC,0xF3, 0x3B, 0x23,0xB8, 0x06,0x00},
	{0xFC,0xF3, 0x3B, 0x22,0xFB, 0x00,0x00},
};

static struct i2c_driver fm11_ge300_driver;
#define       reg_val_length    (sizeof(reg_val)/sizeof(reg_val[0]))

struct fm11_ge300 {
	int num;
};

static int fm11_ge300_i2c_write(struct i2c_adapter *i2c_adap, unsigned char address,
                            unsigned int len, unsigned char const *data)
{
        struct i2c_msg msgs[1];
        int res;
	int i = 0;
	int m = 0;

        if (!data || !i2c_adap) {
                printk("%s:line=%d,error\n",__func__,__LINE__);
                return -EINVAL;
        }
	//for(i = 0;i < len; i++)
	//	printk("ssssssssssss = %02x\t",data[i]);
	//
	//printk("\n");
	//printk("len              =========================== %d\n",len);
	
        msgs[0].addr = address;
        msgs[0].flags = 0;      /* write */
        msgs[0].buf = (unsigned char *)data;
        msgs[0].len = len;
	msgs[0].scl_rate = 100000;

        res = i2c_transfer(i2c_adap, msgs, 1);
        if (res == 1)
                return 0;
        else if(res == 0)
                return -EBUSY;
        else
                return res;

}

#define reg_addr sizeof(reg_val[0])-2
static int fm11_ge300_i2c_read(struct i2c_adapter *i2c_adap, unsigned char address,
				unsigned short int reg, unsigned int len, unsigned char *data)
{
        struct i2c_msg msgs[2];
        int res,i = 0;
	char bbuuff[1] = {0};
#if 1
	unsigned char buf[reg_addr];
	for(i=0 ; i< reg_addr-2; i++){
		buf[i] = reg_val[0][i];
	}
	printk("sssssssssssssssssss   reg_addr  = %d   read reg = %02x\n",reg_addr,buf[reg_addr-3]);
	buf[reg_addr-3] = buf[reg_addr-3] & 0xf3;
	buf[reg_addr-3] = buf[reg_addr-3] | 0x04;
	buf[reg_addr-2] = (reg & 0xff00) >> 8;
	buf[reg_addr-1] = reg & 0xff;
	printk("fm11_ge300_i2c_read   read reg = %02x%02x\n",buf[reg_addr-2],buf[reg_addr-1]);
	printk("fm11_ge300_i2c_read   read reg = %02x\n",buf[reg_addr-3]);
//	fm11_ge300_i2c_write(i2c_adap,address,reg_addr-2,buf);
	
	
	printk("buf[0] = %02x ,buf[1] = %02x ,buf[2] = %02x ,buf[3] = %02x ,buf[4] = %02x \n",buf[0],buf[1],buf[2],buf[3],buf[4]);
	fm11_ge300_i2c_write(i2c_adap,address,5,buf);

        if (!data || !i2c_adap) {
                printk("%s:line=%d,error\n",__func__,__LINE__);
                return -EINVAL;
        }
	buf[2] = 0x60;
	buf[3] = 0x25;

        msgs[0].addr = address;
        msgs[0].flags = 0;      // write  
        msgs[0].buf = buf;
        msgs[0].len = 4;
	msgs[0].scl_rate = 100000;
#endif

//	fm11_ge300_i2c_write(i2c_adap,address,5,buf);
#if 0
        msgs[1].addr = address;
        msgs[1].flags = I2C_M_RD;
        msgs[1].buf = data;
        msgs[1].len = len;
	msgs[1].scl_rate = 100000;

        res = i2c_transfer(i2c_adap, msgs, 2);
        if (res == 2)
                return 0;
        else if(res == 0)
                return -EBUSY;
        else
                return res;
#endif
	bbuuff[0] = 0xc1;
        msgs[1].addr = address;
        msgs[1].flags = I2C_M_RD;
        msgs[1].buf = bbuuff;
        msgs[1].len = 1;
	msgs[1].scl_rate = 100000;

        res = i2c_transfer(i2c_adap, msgs, 2);
	printk("rrrrrrrrrrrrrrrrrrrr  read reg = %02x%02x\n",data[0],data[1]);
        if (res == 2)
                return 0;
        else if(res == 0)
                return -EBUSY;
        else
                return res;

}

static int fm11_ge300_init(struct i2c_client *client)
{
	int i = 0;
	char data[2]={0};
	//printk("start fm11 init\n");
	for(i = 0; i < reg_val_length; i++){
		if(0 > fm11_ge300_i2c_write(client->adapter, client->addr,sizeof(reg_val[0]),reg_val[i])){
			printk("fm11_ge300_i2c_write  error\n");
			return -1;
		}
	}
	//printk("end fm11 init\n");
	
	//msleep(50);
	//for(i = 0; i < reg_val_length; i++){
	//	if(0 <= fm11_ge300_i2c_read(client->adapter, client->addr,(reg_val[i][3]<<8 | reg_val[i][4]),2,data)){
	//		printk("fm11_ge300_i2c_read   read reg = %02x%02x value = %02x%02x\n",reg_val[i][3],reg_val[i][4],data[0],data[1]);
	//	}
	//}
	
	return 0;
}

static int fm11_ge300_probe(struct i2c_client *client,
				const struct i2c_device_id *id)
{
	struct fm11_ge300 *fm11_ge300;
	int gpio_num = -1;
	int ret = 0;
	struct device_node *np = client->dev.of_node;

        //gpio_num = of_get_named_gpio_flags(np, "rst_gpio", 0, NULL);
        //if (!gpio_is_valid(gpio_num))
        //        gpio_num = -1;

	//printk("rst = = = = = = = %d\n",gpio_num);
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
		return -ENODEV;

	fm11_ge300 = devm_kzalloc(&client->dev, sizeof(struct fm11_ge300),GFP_KERNEL);
	if (!fm11_ge300)
                return -ENOMEM;

        i2c_set_clientdata(client, fm11_ge300);

	//ret = gpio_request(gpio_num, "rst_gpio");
	//if (ret < 0){
        //                printk("rst_gpio request error\n");
        //        }
	//gpio_direction_output(gpio_num, 0);
        //gpio_set_value(gpio_num, 0);
	//msleep(10);
	//gpio_direction_output(gpio_num, 1);
        //gpio_set_value(gpio_num, 1);
	//msleep(10);
	if(0 > fm11_ge300_init(client))
		printk("fm11_ge300_probe  error\n");

	return 0;
}

static int fm11_ge300_remove(struct i2c_client *client)
{
	return 0;
}

static const struct i2c_device_id fm11_ge300_id[] = {
	{ "fm11_ge300", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, fm11_ge300_id);

#ifdef CONFIG_OF
static const struct of_device_id fm11_ge300_of_match[] = {
	{ .compatible = "fm11ge300" },
	{}
};
MODULE_DEVICE_TABLE(of, fm11_ge300_of_match);
#endif

static struct i2c_driver fm11_ge300_driver = {
	.driver		= {
		.name	= "fm11_ge300",
		.owner	= THIS_MODULE,
		.of_match_table = of_match_ptr(fm11_ge300_of_match),
	},
	.probe		= fm11_ge300_probe,
	.remove		= fm11_ge300_remove,
	.id_table	= fm11_ge300_id,
};

module_i2c_driver(fm11_ge300_driver);

MODULE_AUTHOR("Alessandro Zummo <a.zummo@towertech.it>");
MODULE_LICENSE("GPL");
