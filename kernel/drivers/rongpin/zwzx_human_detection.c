/*
 * lcd backlight pin ctl driver  
 * Data: 20180725
 */
#include<linux/device.h>

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/device.h>
#include <linux/gpio.h>
#include "rpdzkj-sysfs.h"
#ifdef CONFIG_OF
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#endif
#include <linux/timer.h>

#include <asm/uaccess.h>

#include <linux/of_gpio.h>
#include <linux/fb.h>
#include <linux/backlight.h>
#include <linux/err.h>
#include <linux/pwm.h>
#include <linux/pwm_backlight.h>
#include <linux/slab.h>

static struct led_data *human_detection_pin = NULL;
static struct led_data *lcd_backlight_pin = NULL;
static struct timer_list HumanDetectionGpioCtlTimer;
//static int led_recovery_flag  = 0;

/*
static ssize_t recovery_write(struct device* dev, struct device_attribute* attr, const char* buf, size_t count){

	led_recovery_flag = buf[0] - 0x30;
	printk("led_flash_status = %d\n",led_recovery_flag);

	if(led_recovery_flag == 2){
		gpio_direction_output(human_detection_pin->led_gpio.gpio,1);
		human_detection_pin->led_gpio.enable = 1;
	}else if(led_recovery_flag == 1){
		gpio_direction_output(human_detection_pin->led_gpio.gpio,0);
		human_detection_pin->led_gpio.enable = 0;
	}
	return count;
}

static ssize_t recovery_read(struct device* dev, struct device_attribute* attr, char* buf){
	printk("led_flash_status = %d\n",led_recovery_flag);
	return 0;
}

static DEVICE_ATTR(recovery, 0666, recovery_read, recovery_write);
*/



void human_detection_ctl_func(unsigned long data)
{
	if(0 == gpio_get_value(human_detection_pin->led_gpio.gpio)){
		lcd_backlight_pin->led_gpio.enable = 1;
        	gpio_direction_output(lcd_backlight_pin->led_gpio.gpio, lcd_backlight_pin->led_gpio.enable);		
	}else{
		lcd_backlight_pin->led_gpio.enable = 0;
        	gpio_direction_output(lcd_backlight_pin->led_gpio.gpio, lcd_backlight_pin->led_gpio.enable);	
	} 

        mod_timer(&HumanDetectionGpioCtlTimer, jiffies + msecs_to_jiffies(2));

        return;
}

static int rp_human_detection_probe(struct platform_device *pdev)
{
	
	int ret;
	int init_flag = 0;

#ifdef CONFIG_OF
	enum of_gpio_flags flags;
	struct device_node *node = pdev->dev.of_node;
#endif

	if(!human_detection_pin){
		human_detection_pin = kzalloc(sizeof(struct led_data), GFP_KERNEL);
		if (!human_detection_pin)
			return -ENOMEM;
		memset(human_detection_pin, 0, sizeof(struct led_data));
	}
	if(!lcd_backlight_pin){
		lcd_backlight_pin = kzalloc(sizeof(struct led_data), GFP_KERNEL);
		if (!lcd_backlight_pin)
			return -ENOMEM;
		memset(lcd_backlight_pin, 0, sizeof(struct led_data));
	}
	if(!node){
		if(!human_detection_pin)
			kfree(human_detection_pin);
		if(!lcd_backlight_pin)
			kfree(lcd_backlight_pin);
		return -1;
	}

#ifdef CONFIG_OF
	human_detection_pin->led_gpio.gpio = of_get_named_gpio_flags(node, "human_detection_pin", 0, &flags);
	if (gpio_is_valid(human_detection_pin->led_gpio.gpio)){
		human_detection_pin->led_gpio.enable = (flags == 1)? 1:0;
		printk("human_detection_pin ggggg gpio:%d, enable:%d\n\n", human_detection_pin->led_gpio.gpio, human_detection_pin->led_gpio.enable);
	}else{
		printk("human_detection_pin invalid gpio: %d\n",human_detection_pin->led_gpio.gpio);
		gpio_free(human_detection_pin->led_gpio.gpio);
	}

	lcd_backlight_pin->led_gpio.gpio = of_get_named_gpio_flags(node, "lcd_backlight_pin", 0, &flags);
	if (gpio_is_valid(lcd_backlight_pin->led_gpio.gpio)){
		lcd_backlight_pin->led_gpio.enable = (flags == 1)? 1:0;
		printk("lcd_backlight_pin ggggg gpio:%d, enable:%d\n\n", lcd_backlight_pin->led_gpio.gpio, lcd_backlight_pin->led_gpio.enable);
	}else{
		printk("lcd_backlight_pin invalid gpio: %d\n",lcd_backlight_pin->led_gpio.gpio);
		gpio_free(lcd_backlight_pin->led_gpio.gpio);
	}
#endif

	gpio_request(lcd_backlight_pin->led_gpio.gpio, "lcd_backlight_pin");
	gpio_request(human_detection_pin->led_gpio.gpio, "human_detection_pin");
//	gpio_direction_output(human_detection_pin->led_gpio.gpio,human_detection_pin->led_gpio.enable);
	
#if 1
	init_timer(&HumanDetectionGpioCtlTimer);
	HumanDetectionGpioCtlTimer.expires = jiffies + jiffies_to_msecs(2);
	HumanDetectionGpioCtlTimer.function = human_detection_ctl_func;
	HumanDetectionGpioCtlTimer.data = 0;
	add_timer(&HumanDetectionGpioCtlTimer);
#endif
	return 0;
}

static int rp_human_detection_remove(struct platform_device *pdev)
{
	del_timer(&HumanDetectionGpioCtlTimer);
	
	gpio_free(human_detection_pin->led_gpio.gpio);
	gpio_free(lcd_backlight_pin->led_gpio.gpio);
	
	if(!human_detection_pin){
		kfree(human_detection_pin);
	}
	if(!lcd_backlight_pin){
		kfree(lcd_backlight_pin);
	}
	
	return 0;
}

static int rp_human_detection_suspend(struct platform_device *pdev, pm_message_t state)
{
	del_timer(&HumanDetectionGpioCtlTimer);
	return 0;
}
static int rp_human_detection_resume(struct platform_device *pdev)
{
	add_timer(&HumanDetectionGpioCtlTimer);
	return 0;
}

#ifdef CONFIG_OF
static struct of_device_id human_detection_pin_dt_ids[] = {
	{ .compatible = "human_detection_ctl" },
	{}
};
MODULE_DEVICE_TABLE(of, human_detection_pin_dt_ids);
#endif
static struct platform_driver rp_human_detection_driver = {
	.driver = {
		.name    = "human_detection_ctl",
		.owner   = THIS_MODULE,
#ifdef CONFIG_OF
		.of_match_table = of_match_ptr(human_detection_pin_dt_ids),
#endif
	},
	.suspend = rp_human_detection_suspend,
	.resume = rp_human_detection_resume,
	.probe		= rp_human_detection_probe,
	.remove		= rp_human_detection_remove,
};

static int __init rp_human_detection_init(void)
{
	return platform_driver_register(&rp_human_detection_driver);
}
subsys_initcall(rp_human_detection_init);

static void __exit rp_human_detection_exit(void)
{
	platform_driver_unregister(&rp_human_detection_driver);
}
module_exit(rp_human_detection_exit);

MODULE_AUTHOR("rpdzkj");
MODULE_DESCRIPTION("human detection ctl Drive for ronpin ");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("platform:HumanDetection-gpio");

